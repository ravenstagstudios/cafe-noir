using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// Utility class containing lots of odds and ends.
/// </summary>
public static class AdventureUtility
{
	#region Layers
	
	
	/// <summary>
	/// The default layer number
	/// </summary>
	public static readonly int LAYER_DEFAULT = 0;
	
	/// <summary>
	/// The layer number that makes an object be ignored
	/// while player is pressing/clicking.
	/// </summary>
	public static readonly int LAYER_IGNORE_RAYCAST = 2;
	
	/// <summary>
	/// The layer number that makes nav mesh in scene
	/// treat area around an object as unwalkable.
	/// </summary>
	public static readonly int LAYER_NAVMESH_COLLIDE = 9;
	
	
	#endregion
	
	
	/// <summary>
	/// Gets mouse position with inverted y-position.
	/// </summary>
	public static Vector2 InvertedMouseY
	{
		get
		{
			return new Vector2(Input.mousePosition.x,
				Screen.height - Input.mousePosition.y);
		}
	}
}

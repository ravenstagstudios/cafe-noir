﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	GameObject player;
	public float offsetY = -1.5f;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		transform.position =(new Vector3(player.rigidbody2D.position.x, offsetY, transform.position.z));
	
	}
}

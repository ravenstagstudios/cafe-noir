﻿using UnityEngine;
using System.Collections;

public class NPCText : MonoBehaviour {

	public GameObject textBubble;
	public GameObject textBubble2;
	GameObject player;
	Inventory inv;
	bool givenCoffee;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		inv = player.GetComponent<Inventory> ();

		textBubble.transform.localScale = (new Vector3(0,0,0));
		textBubble2.transform.localScale = (new Vector3(0,0,0));
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Mathf.Abs (player.rigidbody2D.position.x - transform.position.x )<5f){
			if(givenCoffee){
				textBubble.transform.localScale = (new Vector3(0,0,0));
				textBubble2.transform.localScale = (new Vector3(1,1,1));
			}
			else{
				textBubble2.transform.localScale = (new Vector3(0,0,0));
				textBubble.transform.localScale = (new Vector3(1,1,1));
			}
		}
		else{
			textBubble.transform.localScale = (new Vector3(0,0,0));
			textBubble2.transform.localScale = (new Vector3(0,0,0));
		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.rigidbody.tag == "Player" && inv.hasCoffee){
			givenCoffee = true;
			collider2D.enabled = false;

		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class NPCBarrista : MonoBehaviour {

	public GameObject textBubble;
	public GameObject textBubble2;
	GameObject player;
	Inventory inv;
	bool givenMoney;

	Transition trans;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		inv = player.GetComponent<Inventory> ();
		trans = player.GetComponent<Transition> ();

		textBubble.transform.localScale = (new Vector3(0,0,0));
		textBubble2.transform.localScale = (new Vector3(0,0,0));
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Mathf.Abs (player.rigidbody2D.position.x - transform.position.x )<2f){
			if(givenMoney){
				textBubble.transform.localScale = (new Vector3(0,0,0));
				textBubble2.transform.localScale = (new Vector3(1,1,1));
			}
			else{
				textBubble2.transform.localScale = (new Vector3(0,0,0));
				textBubble.transform.localScale = (new Vector3(1,1,1));
			}
		}
		else{
			textBubble.transform.localScale = (new Vector3(0,0,0));
			textBubble2.transform.localScale = (new Vector3(0,0,0));
		}
	}


	void OnCollisionEnter2D(Collision2D trig){
		if (trig.rigidbody.tag == "Player"){
			trans.talkBarrista = true;
			if(inv.money == 5){
				inv.money = 0;
				trans.talkBarrista = true;
				givenMoney = true;
				collider2D.enabled = false;
			}
		}
	}
}

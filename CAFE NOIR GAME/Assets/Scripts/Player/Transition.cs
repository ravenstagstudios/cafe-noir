﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {

	string previousLevel;

	GameObject spawn;

	Inventory inv;

	//Progress Checkpoints
	public bool talkBarrista = false;


	// Use this for initialization
	void Awake () {
		Debug.Log ("Test");
		DontDestroyOnLoad(transform.gameObject);
		inv = this.GetComponent<Inventory>();
		previousLevel = Application.loadedLevelName;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnLevelWasLoaded(){
		Debug.Log ("Loaded");

		if(Application.loadedLevelName == "Alleyway"){
			transform.position = new Vector3(-62, -4.6f, -1);
		}

		if(Application.loadedLevelName == "Street"){
			if(previousLevel == ("Alleyway") || previousLevel == ("TheFix")){
				transform.position = new Vector3(-20, -5, -1);
			}
			else if (previousLevel == "House"){
				spawn = GameObject.Find ("House>Street Spawner");
			}
		}

		if(Application.loadedLevelName == "Street2"){
			if(previousLevel == ("Alleyway") || previousLevel == ("TheFix")){
				transform.position = new Vector3(-20, -5, -1);
			}
			else if (previousLevel == "House"){
				spawn = GameObject.Find ("House>Street2 Spawner");
			}
		}

		if(Application.loadedLevelName == "TheFix"){
			rigidbody2D.position = new Vector3(-59, -4.4f, -1);
		}

		//Debug.Log (spawn.transform.position.x);
		//transform.position = new Vector3 (spawn.transform.position.x, spawn.transform.position.y, 0f);

		previousLevel = Application.loadedLevelName;
	}

	void OnTriggerEnter2D(Collider2D trig) {
		Debug.Log ("Triggering with "+trig.name);
		if (trig.transform.tag == "Alleyway>Street"){
			Debug.Log ("test");
			if (talkBarrista){
				Debug.Log ("test1");
				Application.LoadLevel("Street2");
			}
			if(talkBarrista == false){
				Debug.Log ("test2");
				Application.LoadLevel("Street");
			}

		}

		if (trig.transform.tag == "Alleyway>CoffeeCo" && inv.hasKey)
			Application.LoadLevel("CoffeeCo");

		if (trig.transform.tag == "TheFix>Street"){
			Debug.Log ("test");
			Application.LoadLevel("Street2");


		}
	}

	


}

﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	GameObject player;
	Animator anim;
	Vector3 mouse;
	float vel;
	float dirVel;
	float pos;
	float deltaPos; //Absolute value of mouse - pos
	float force;
	float lowVelThreshold = 4f;
	float midVelThreshold = 5f;
	float highVelThreshold = 6f;
	float rangeThreshold = 10f;
	float stopThreshold = 1f;
	public float forceMultiplier = 1.5f;
	float minForce = 4;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		anim = player.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		mouse = camera.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0));
		vel = player.rigidbody2D.velocity.x;
		pos = player.rigidbody2D.position.x;
		deltaPos = Mathf.Abs (mouse.x - pos);

		Debug.Log ("Deltapos = " + deltaPos);
		Debug.Log ("Dir Vel = " + dirVel);
		anim.SetFloat ("Speed", Mathf.Abs(vel));
		if (vel >0.1){
			player.transform.localScale = new Vector3(-1, 1, 1);
			dirVel = vel;
		}
		else if (vel <-0.1){
			player.transform.localScale = new Vector3(1, 1, 1);
			dirVel = vel * (-1);
		}
		CompareSpeed ();

		//Invert scale to face direction of travel





	}

	void CompareSpeed(){


		if(deltaPos < 1){
			if(dirVel > midVelThreshold){
				force = -4;
			}
			else if(dirVel > lowVelThreshold){
				force = -2;
			}
			else if(dirVel < lowVelThreshold){
				force = 0;
			}

			if((mouse.x - pos) * vel < 0)
				force = -force;
		}



		else if (deltaPos < 4 && deltaPos >= 1){
			if(dirVel > midVelThreshold){
				force = 0;
			}
			else if(dirVel > lowVelThreshold && dirVel <= midVelThreshold){
				force = -2;
			}
			else{
				force = 5;
			}
		}
		else if (deltaPos >= 4){
			if(dirVel < highVelThreshold){
				force = 5;
			}
			else{
				force = 0;
			}
		}

		if(mouse.x - pos < -1)
			force = -force;



		player.rigidbody2D.AddForce(new Vector2(force * forceMultiplier, 0));
		/*

		if(mouse.x - pos < stopThreshold && mouse.x - pos > stopThreshold)
			force = 0;

		else if (pos > mouse.x) {
			if(pos - mouse.x < rangeThreshold){
				force = pos - mouse.x;
				if (force < minForce)
					force = minForce;
				if(vel < -velThreshold){
					force = force + vel;
				}
				else if(vel > 2)
					force = 10;
			}
			else{
				force = rangeThreshold;
			}
		}
		else if (pos < mouse.x){
			if(mouse.x - pos < rangeThreshold){
				force = pos - mouse.x;
				if (force > -minForce)
					force = -minForce;
				if(vel > velThreshold){
					force = force - vel;
				}
				else if(vel < -2)
					force = -10;
			}
			else{
				force = -rangeThreshold;
			}
		}
		player.rigidbody2D.AddForce(new Vector2(-force * forceMultiplier, 0));
		*/
	}
}

﻿using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour {

	RaycastHit2D selected;
	GameObject player;
	bool grabbing = false; 
	bool ladderClimbR = false;
	bool ladderClimbL = false;


	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {


		if(Input.GetMouseButtonDown(0)){
			selected = Physics2D.Raycast(Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);


			Debug.Log ("checking for mouse triggered transition");
			if (selected){
				if(selected.transform.tag == "Street>TheFix")
					Application.LoadLevel("TheFix");
				if(selected.transform.tag == "Street>Alleyway")
					Application.LoadLevel("Alleyway");
				if(selected.transform.tag == "Street2>TheFix")
					Application.LoadLevel("TheFix");
				if(selected.transform.tag == "Street2>Alleyway")
					Application.LoadLevel("Alleyway");



				if(selected.rigidbody.tag == "Pushable" || selected.rigidbody.tag == "Ladder" || selected.rigidbody.tag == "NoClimb"){
					if(Mathf.Abs (selected.rigidbody.position.x - player.rigidbody2D.position.x) < 3f){
						grabbing = true;
					}
				}
			}

		}

		if(Input.GetMouseButtonUp(0)){
			grabbing = false;
		}
		//Debug.Log (grabbing);
		if(grabbing){
			selected.rigidbody.mass = 0.01f;
		}
		else{
			if(selected)
				selected.rigidbody.mass = 100f;
		}



		//Right Click to Climb
		if(Input.GetMouseButton (1)){
			selected = Physics2D.Raycast(Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (selected){

				if(selected.rigidbody.tag == "Ladder"){
					if((player.rigidbody2D.position.y - selected.rigidbody.position.y)<4.5f && Mathf.Abs (selected.rigidbody.position.x - player.rigidbody2D.position.x) < 2f){
						if(ladderClimbR == false && ladderClimbL == false){
							if(player.rigidbody2D.position.x < selected.rigidbody.position.x)
								ladderClimbR = true;
							else
								ladderClimbL = true;
						}
					}
				}




				if((player.rigidbody2D.position.y - selected.rigidbody.position.y)<2.5f && Mathf.Abs (selected.rigidbody.position.x - player.rigidbody2D.position.x) < 3f && selected.rigidbody.tag == "Pushable"){
					selected = Physics2D.Raycast(Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
					if(player.rigidbody2D.position.x < selected.rigidbody.position.x){
						player.rigidbody2D.AddForce(new Vector2(2, 30));
					}
						
					if(player.rigidbody2D.position.x > selected.rigidbody.position.x){
						player.rigidbody2D.AddForce(new Vector2(-2, 30));
					}
				}
			}
		}
		else{

			ladderClimbR = false;
			ladderClimbL = false;
		}
		if(ladderClimbL)
			player.rigidbody2D.AddForce(new Vector2(-2, 30));
		else if(ladderClimbR)
			player.rigidbody2D.AddForce(new Vector2(2, 20));

	}
	/*
	void OnCollisionStay2D(Collision2D coll) {
		Debug.Log ("1");
		if (Input.GetMouseButtonDown (1) && coll.rigidbody.tag == "Ladder"){
			Debug.Log ("2");
			if(coll.rigidbody.position.x > this.rigidbody2D.position.x)
				this.rigidbody2D.AddForce(new Vector2(20, 10000));
			else
				this.rigidbody2D.AddForce(new Vector2(-20, 3000));
		}
	}
	*/

}

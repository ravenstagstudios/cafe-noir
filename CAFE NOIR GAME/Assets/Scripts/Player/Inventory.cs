﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {

	public int money;
	public bool hasCoffee;
	public GameObject coffee;
	public bool hasKey = false;
	public GameObject key;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//Key
		if(hasKey){
			key.transform.localScale = (new Vector3(1,1,1));
			key.transform.position = new Vector3(transform.position.x,transform.position.y + 2f,0);
		}
		else{
			key.transform.localScale = (new Vector3(0,0,0));
		}

		//Coffee
		if(hasCoffee){
			coffee.transform.localScale = (new Vector3(1,1,1));
			coffee.transform.position = new Vector3(transform.position.x,transform.position.y + 2f,0);
		}
		else{
			coffee.transform.localScale = (new Vector3(0,0,0));
		}
	
	}

	void OnTriggerEnter2D(Collider2D trig) {

		//Key
		if (trig.transform.tag == "Key"){
			hasKey = true;
			Destroy(trig.gameObject);
		}


		//Coffee
		if (trig.transform.tag == "Coffee"){
			hasCoffee = true;
			Destroy(trig.gameObject);
		}

		//Money
		if (trig.transform.tag == "Money"){
			money++;
			Destroy(trig.gameObject);
		}
	}


}
